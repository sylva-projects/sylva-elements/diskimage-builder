$schema: http://json-schema.org/draft-07/schema#
definitions:
  container_registries_definition:
    type: object
    description: |
      **Deprecated** specification of registries used by docker and container runtimes.
      Replaced by container_registries
    properties:
      insecure_registries:
        type: array
        description: list of insecure registries that are accepted by the container engine
        items:
          type: string
      registry_mirrors:
        type: array
        description: list of mirrors used
        items:
          type: string
      auths:
        type: array
        description: list of authentication credentials
        items:
          $ref: '#/definitions/registry_auth_definition'
  registry_auth_definition:
    type: object
    description: |
      authentication to a given registry
    additionalProperties: false
    properties:
      repository:
        type: string
        description: name of private repository
      username:
        type: string
        description: name of user connecting to the private repository
      password:
        type: string
        description: password of the authenticated user

  container_registries_definition_v2:
    description: |
      Specification of registries used by docker and container runtimes.
      Mapping between names and servers are separated from server
      specification.
    type: object
    properties:
      map:
        description: |
          Specify how names are mapped to server URL
        type: array
        items:
          $ref: '#/definitions/registry_definition'
      servers:
        description: |
          Configuration to access a given server.
        type: array
        items:
          $ref: '#/definitions/registry_server_definition'
      default_mirrors:
        description: |
          List of default mirrors for containerd or docker
          specified by urls.
          It seems hard to emulate with cri-o registry
          configuration.
        type: array
        items:
          type: string

  registry_definition:
    type: object
    properties:
      name:
        description: external name of registry
        type: string
      server:
        description: URL of default server used for this registry if not name
        type: string
      mirrors:
        description: List of mirrors that can be used
        type: array
        items:
          type: string
  registry_server_definition:
    type: object
    properties:
      url:
        description: |
          url of the server. It must match the server field used in map.
        type: string
      username:
        type: string
        description: name of user connecting to the private repository
      password:
        type: string
        description: password of the authenticated user
      insecure:
        desription: whether to accept insecure TLS connection
        type: boolean
      override_path:
        description: |
           whether to use the URL path in addition of the host endpoint (only supported by containerd)
        type: boolean
      ca:
        type: string
        description: CA to use to validate the TLS connection
      client_cert:
        type: string
        description: |
          A client certificate in PEM format for authentication on
          the registry.
      client_key:
        type: string
        description: |
          A client key in PEM format for authentication on the registry

properties:
  containers:
    $ref: '#/definitions/container_registries_definition'
  container_registries:
    $ref: '#/definitions/container_registries_definition_v2'
